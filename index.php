<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="assets/style.css">
    <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah" rel="stylesheet"><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>TodoList</title>
</head>
<body>
    <?php
        include("classes/Todo.php");
        include("classes/Db.php");
    ?>
    <div id="postIt">
        <img src="assets/punaise.png" class="imgPunaise" alt="punaise">
        <h3>Todo List</h3>
        <div class="container">
            <?php

                //      Récupère les Todo en bdd et affiche
                $bdd = Db::connection();
                $request = $bdd->query('SELECT * FROM todos');
                $todos = $request->fetchAll();

                foreach($todos as $todoArray){
                    //      Nouveau Todo (objet)
                    $todo = new Todo($todoArray['text'], $todoArray['done'], $todoArray['id']);
            ?>
            <div class="containerTodo">
                <form method="post" action="forms/updateTodo.php">
                    <input type="checkbox" class="checkBox" name="done" <?php echo 'value="' .$todo->getDone() .'"'?><?php if($todo->getDone() == 1) echo 'checked'?>/>
                    <input class="Text-box" type="text" name="text" <?php echo 'value="'. $todo->getText() .'"'?>/>
                    <input class="imgSave" type="image" src="assets/save.png" name="save" value="submit" />
                    <input name="id" type="hidden"  <?php echo 'value="'. $todo->getId() .'"'?>/>
                </form>
                <form method="post" action="forms/deleteTodo.php">
                    <input name="id" type="hidden"  <?php echo 'value="'. $todo->getId() .'"'?>/>
                    <input class="imgDelete" type="image" src="assets/imgDelete.png"/>
                </form>
            </div>
                <?php
                }
                ?>
            </div>
            <h2>Add to the Todo List:</h2>
            <div class="containerAdd">
                <form method="post" action="forms/addTodo.php">
                    <input class="Text-boxAdd" type="text" name="text" placeholder="todo" maxlength="18"></input>
                    <input  class="imgAdd" type="image" src="assets/add.png" value="submit" name="add"/>
                </form>
            </div>
        </div>
</body>
</html>