<?php
    include('../classes/Todo.php');
    include('../classes/Db.php');

    //      Défini la variable id
    $id = $_POST['id'];

    //      Suppression du Todo sélectionné grâce a son ID dans la bdd
    Db::deleteTodo($id);

    header('Location: ' . $_SERVER['HTTP_REFERER']);
?>