<?php
    include("../classes/Todo.php");
    include("../classes/Db.php");

    //      Défini la variable text
    $text = $_POST['text'];

    //      Ajout du Todo dans la bdd
    Db::addTodo($text);

    header('Location: ' . $_SERVER['HTTP_REFERER']);
?>