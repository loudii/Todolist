<?php
    include('../classes/Todo.php');
    include('../classes/Db.php');

    //      Défini les variables id, done et text
    $id = $_POST['id'];
    $done = $_POST['done'];
    $text = $_POST['text'];

    //      Précise si done est défini, puis lui donne 1 ou 0
    if(isset($done)){
        $done = 1;
    }
    else{
        $done = 0;
    }

    //      Modification du Todo sélectionné grâce a son ID dans la bdd
    Db::updateTodo($id, $done, $text);

    header('Location: ' . $_SERVER['HTTP_REFERER']);
?>