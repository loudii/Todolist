<?php
    class Todo {
        public $text;
        private $done;
        private $id;

        public function __construct($text, $done = false, $id){
            $this->setText($text);
            $this->setDone($done);
            if (isset($id)) { $this->id = $id; }
        }

        //      Récupère la variable Id
        public function getId(){
            return $this->id;
        }

        //      Récupère la variable Text
        public function getText(){
            return $this->text;
        }

        //      Permet de modifier la valeur de la variable Text
        public function setText($valueText){
            $this->text = $valueText;
        }

        //      Récupère la variable Done
        public function getDone(){
            return $this->done;
        }

        //      Permet de modifier la valeur de la variable Done
        public function setDone($valueDone){
            $this->done = $valueDone;
        }
    }
?>