<?php
    class Db {

        //      Lien entre le fichier et la BDD
        static function connection(){
            $user = 'root';
            $password = 'seanoirmam';
            $host = 'localhost' ;
            $database = 'TodoList';
            $bdd = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $user, $password);
            $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $bdd;
        }

        //      Function pour ajouter un Todo à la BDD
        static function addTodo($text){
            $bdd = self::connection();
            $request = $bdd->prepare("INSERT INTO todos VALUES(NULL, :text, 0)");
            $request->execute(['text' => $text]);
        }

        //      Function pour supprimer un Todo à la BDD
        static function deleteTodo($id){
            $bdd = self::connection();
            $request = $bdd->prepare('DELETE FROM todos WHERE id = :id');
            $response = $request->execute(['id' => $id]);
        }

        //      Function pour modifier un Todo à la BDD
        static function updateTodo($id, $done, $text){
            $bdd = self::connection();
            $request = $bdd->prepare("UPDATE todos SET text = :text, done = :done WHERE id = :id");
            $request->execute(['text' => $text, 'done' => $done, 'id'=>$id]);
        }
    }
?>